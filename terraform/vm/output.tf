# Virtal Machine Details
output "virtual_machine_id" {
  value = azurerm_linux_virtual_machine.linux.id
}

output "virtual_machine_name" {
  value = azurerm_linux_virtual_machine.linux.name
}

output "virtual_machine_private_ip" {
  value = azurerm_network_interface.dynamic.private_ip_address
}

# Interface id
output "azurerm_network_interface_id" {
  value = azurerm_network_interface.dynamic.id
}

# Credentials
output "admin_username" {
  value = random_string.username.result
}

output "admin_password" {
  value     = random_password.password.result
  sensitive = true
}

output "public_ip_address" {
    value = azurerm_public_ip.primary[0].ip_address
}