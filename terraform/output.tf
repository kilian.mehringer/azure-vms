output "vm_admin_username" {
  value     = [for vm in module.linux_virtual_machine : vm.admin_username[*]]
  sensitive = true
}

output "vm_admin_password" {
  value     = [for vm in module.linux_virtual_machine : vm.admin_password[*]]
  sensitive = true
}

output "public_ip_adress" {
  value = [for vm in module.linux_virtual_machine : vm.public_ip_address[*]]
}

data "local_file" "healthcheck_file" {
  filename = "healthchecks"
}

output "healthcheck" {
    value = data.local_file.healthcheck_file.content
}
