resource_group_name     = "rg-bootcamp-kilian-mehringer"
resource_group_location = "East US"
virtual_machine_size    = "Standard_B1s"
image_publisher         = "Canonical"
image_offer             = "UbuntuServer"
image_sku               = "18.04-LTS"
image_version           = "latest"
vm_count                = 2