
#!/bin/bash
IFS=',' read -r -a ip_array <<< "$IP_ADRESSES"

len=${#ip_array[@]}

for (( i=0; i<$len; i++ ))
do 
 ping -c 1 ${ip_array[$i]} > healthchecks
done