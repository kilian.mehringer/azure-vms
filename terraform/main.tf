terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.70.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
  required_version = ">=0.15.0"
}

provider "azurerm" {
  features {}
}

resource "random_string" "random" {
  length  = 12
  upper   = false
  special = false
}


data "azurerm_subscription" "current" {
}

module "subscription" {
  source          = "github.com/Azure-Terraform/terraform-azurerm-subscription-data.git?ref=v1.0.0"
  subscription_id = data.azurerm_subscription.current.subscription_id
}

module "naming" {
  source = "github.com/Azure-Terraform/example-naming-template.git?ref=v1.0.0"
}

module "metadata" {
  source = "github.com/Azure-Terraform/terraform-azurerm-metadata.git?ref=v1.1.0"

  naming_rules = module.naming.yaml

  market              = "us"
  project             = "https://github.com/Azure-Terraform/terraform-azurerm-virtual-machine/tree/main/examples"
  location            = "eastus2"
  environment         = "sandbox"
  product_name        = random_string.random.result
  business_unit       = "infra"
  product_group       = "contoso"
  subscription_id     = module.subscription.output.subscription_id
  subscription_type   = "dev"
  resource_group_type = "app"
}

module "resource_group" {
  source = "github.com/Azure-Terraform/terraform-azurerm-resource-group.git?ref=v1.0.0"

  location = module.metadata.location
  names    = module.metadata.names
  tags     = module.metadata.tags
}

module "virtual_network" {
  source = "github.com/Azure-Terraform/terraform-azurerm-virtual-network.git?ref=v2.5.1"

  naming_rules = module.naming.yaml

  resource_group_name = module.resource_group.name
  location            = module.resource_group.location
  names               = module.metadata.names
  tags                = module.metadata.tags

  address_space = ["10.1.0.0/22"]

  subnets = {
    "iaas-private" = { cidrs = ["10.1.1.0/24"]
      allow_vnet_inbound  = true
      allow_vnet_outbound = true
    }
    "iaas-public" = { cidrs = ["10.1.0.0/24"]
      allow_vnet_inbound  = true
      allow_vnet_outbound = true
    }
  }
}

resource "azurerm_proximity_placement_group" "primary" {
  name                = "example-placement-group"
  location            = module.resource_group.location
  resource_group_name = module.resource_group.name
  tags                = module.metadata.tags
}

module "linux_virtual_machine" {
  source = "./vm"

  # Create two VMs
  count = var.vm_count

  resource_group_name = module.resource_group.name
  location            = module.resource_group.location
  names               = module.metadata.names
  tags                = module.metadata.tags
  machine_count       = count.index
  public_ip_enabled   = true
  vm_count            = var.vm_count

  # Windows or Linux?
  kernel_type = "linux"

  # Instance Size
  virtual_machine_size = "Standard_B1s"

  # Operating System Image
  source_image_publisher = var.image_publisher
  source_image_offer     = var.image_offer
  source_image_sku       = var.image_sku
  source_image_version   = var.image_version

  # Virtual Network
  subnet_id = module.virtual_network.subnets["iaas-private"].id

  # Networking
  accelerated_networking    = false
  proximity_placement_group = azurerm_proximity_placement_group.primary.id

}


resource "null_resource" "shell" {

  provisioner "local-exec" {
    command = "bash ./healthchecks.sh"
    environment = {
      IP_ADRESSES = join(",", flatten([for vm in module.linux_virtual_machine : vm.public_ip_address[*]]))
    }
  }
}
