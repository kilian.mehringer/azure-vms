# Azure Vm cluster deployment

This is a terraform deployment for a virtual machine cluster on azure of variable size. 
The vms are created inside one virtual private network in the same placement group and region. 

Additionally the falvor and image of the vms can be adjusted by variables as well as other options

## Variables 

The availibel variables to customize the vm deployment are as follows: 

#### resource_group_name

The resource group to deploy the virtual machines into

#### resource_group_location

The regional location in which the resources will be created. The default is East US as it is one of the most cost effective region in azure

#### virtual_machine_size

The size name of the virtual machine to deploy. Availible sizes can be listed with: 

```
az vm list-skus --location <region> --all --output table
```

Default is the smallest availible unit for testing purposes

#### image_publisher

The publisher of the image to run in the vm 

#### image_offer

The offer of the image to run in the vm 

#### image_sku

The sku of the image to run in the vm 

#### image_version

The version of the image to use

#### vm_count

The amount of vms to spawn 

## Testing

The testing is done by pinging all virtual machine public ips adresses after they are deployed. A Round Robin approch like suggested would have bin vaforable only when the testing can heppen purely from inside the vpn when all vms have no public ip adresses. 

Setting up a vpn connection from the local machine that executes the terraform scripts would have bin a way bigger overhead then assigning public ips to the vms. 

This can be improved by only assigning a ip adresses to oen vm while testing with a terratest module, running the round robin test, while collecting and sending the ping results from all virtual machines to be read from the first one again when the round robin was succesful and return the results to terraform from the vm with a public ip. 

Right now the test results are exported to a healthcheck file and read into an output variable from there. 